import time,board,busio
import numpy as np
import adafruit_mlx90640
from PIL import Image as im
import asyncio
import websockets
import threading

i2c = busio.I2C(board.SCL, board.SDA, frequency=400000) # setup I2C
mlx = adafruit_mlx90640.MLX90640(i2c) # begin MLX90640 with I2C comm
mlx.refresh_rate = adafruit_mlx90640.RefreshRate.REFRESH_16_HZ # set refresh rate

CONNECTIONS = set()
frame = np.zeros((24*32,)) # setup array for storing all 768 temperatures

async def handler2(websocket):
    CONNECTIONS.add(websocket)
    try:
        await websocket.wait_closed()
    finally:
        CONNECTIONS.remove(websocket)

async def handler(websocket):
    CONNECTIONS.add(websocket)
    print('client added')
    try:
        print('wait_closed')
        await websocket.wait_closed()
    finally:
        print('client closed')
        CONNECTIONS.remove(websocket)

async def main():
    async with websockets.serve(handler2, "0.0.0.0", 8765):
        await asyncio.Future()  # run forever

def worker():
    while True:
        try:
           mlx.getFrame(frame)
           websockets.broadcast(CONNECTIONS, frame.tobytes())
        except ValueError:
           continue # if error, just read again
        except RuntimeError:
           continue
    return

threads = []
t = threading.Thread(target=worker)
threads.append(t)
t.start()

asyncio.run(main())

exit(1)

print('Looping')
while True:
    try:
        mlx.getFrame(frame)
        websockets.broadcast(CONNECTIONS, frame.tobytes())
    except ValueError:
        continue # if error, just read again
